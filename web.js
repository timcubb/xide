var express = require('express'), 
    app = express.createServer(express.logger()),
    start = new Date(), 
    port = process.env.PORT || 3000;


app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
  
  res.send('coming soon!');

});


app.listen(port, function() {
  console.log('Listening on:', port);
});


