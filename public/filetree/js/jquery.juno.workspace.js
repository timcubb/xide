


if(jQuery) (function($){
	
	$.extend($.fn, {
		workspace: function(o, h) {
			// Defaults
			if( !o ) var o = {};
			if( o.root == undefined ) o.root = '~/go';
			if( o.folderEvent == undefined ) o.folderEvent = 'click';
			if( o.multiFolder == undefined ) o.multiFolder = true;
			if( o.loadMessage == undefined ) o.loadMessage = 'Loading...';
			
			$(this).each( function() {
				
				function showTree(c, t) {
					$(c).addClass('wait');
					$(".jqueryFileTree.start").remove();
						
					$(c).find('.start').html('');
					//var data = "<ul class='jqueryFileTree' style=''><li class='directory collapsed'><a href='#' rel='../../demo/connectors/'>connectors</a></li><li class='directory collapsed'><a href='#' rel='../../demo/demo/'>demo</a></li><li class='directory collapsed'><a href='#' rel='../../demo/images/'>images</a></li><li class='file ext_html'><a href='#' rel='../../demo/index.html'>index.html</a></li><li class='file ext_js'><a href='#' rel='../../demo/jquery.easing.js'>jquery.easing.js</a></li><li class='file ext_js'><a href='#' rel='../../demo/jquery.js'>jquery.js</a></li><li class='file ext_css'><a href='#' rel='../../demo/jqueryFileTree.css'>jqueryFileTree.css</a></li><li class='file ext_js'><a href='#' rel='../../demo/jqueryFileTree.js'>jqueryFileTree.js</a></li></ul>";
					
					//var data_json = getDirectory(o.root); this call needs to ajax to the server and return the results in this format:

					var data_json = [{
						name: 'bin',
						path: '../../demo/bin/',
						type: 'directory',
						collapsed: true
					},{
						name: 'src',
						path: '../../demo/src/',
						type: 'directory',
						collapsed: false
					},{
						name: 'index.html',
						path: '../../demo/index.html',
						type: 'file',
						ext: 'html'
					},{
						name: 'script.js',
						path: '../../demo/script.js',
						type: 'file',
						ext: 'js'
					},{
						name: 'styles.css',
						path: '../../demo/styles.css',
						type: 'file',
						ext: 'css'
					}];
					

					var data = "<ul class=\"jqueryFileTree\" style=\"display:none\">";
					data += "<li class='directory'><a href='#' rel='..'>..</a></li>";
					
					$.each(data_json, function(k, v) {
						if(v.type == 'file') {
							data += "<li class='file ext_"+v.ext+"'><a href='#' rel='"+v.path+"'>"+v.name+"</a></li>";
						} else {
							var col = (v.collapsed)?' collapsed':'';
							data += "<li class='directory"+col+"'><a href='#' rel='"+v.path+"'>"+v.name+"</a>";
						}
					});


					data += "</ul>";

					$(c).removeClass('wait').append(data);
					if( o.root == t ) $(c).find('UL:hidden').show(); else $(c).find('UL:hidden').slideDown({ duration: o.expandSpeed, easing: o.expandEasing });
					//bindTree(c);
				}
				
				function bindTree(t) {
					$(t).find('LI A').bind(o.folderEvent, function() {
						if( $(this).parent().hasClass('directory') ) {
							if( $(this).parent().hasClass('collapsed') ) {
								// Expand
								if( !o.multiFolder ) {
									$(this).parent().parent().find('UL').slideUp({ duration: o.collapseSpeed, easing: o.collapseEasing });
									$(this).parent().parent().find('LI.directory').removeClass('expanded').addClass('collapsed');
								}
								$(this).parent().find('UL').remove(); // cleanup
								showTree( $(this).parent(), escape($(this).attr('rel').match( /.*\// )) );
								$(this).parent().removeClass('collapsed').addClass('expanded');
							} else {
								// Collapse
								$(this).parent().find('UL').slideUp({ duration: o.collapseSpeed, easing: o.collapseEasing });
								$(this).parent().removeClass('expanded').addClass('collapsed');
							}
						} else {
							h($(this).attr('rel'));
						}
						return false;
					});
					// Prevent A from triggering the # on non-click events
					if( o.folderEvent.toLowerCase != 'click' ) $(t).find('LI A').bind('click', function() { return false; });
				}
				// Loading message
				$(this).html('<ul class="jqueryFileTree start"><li class="wait">' + o.loadMessage + '<li></ul>');
				// Get the initial file list
				showTree( $(this), escape(o.root) );
			});
		}
	});
	
})(jQuery);