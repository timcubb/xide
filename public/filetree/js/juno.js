setInterval(function() {
  Juno.log('Saving changes to files:');
  $.each(Juno.files, function(k,v) {
    Juno.log(v.doc);
  });
}, (5*60*1000));


// Juno framework
var Juno = {
    files: [],
    file: {
        new: function(file) {
          Juno.log("Registering document: "+file.doc);
          file.buffer = file.ref.getValue();
          Juno.files.push(file);
          $(document).bind('keyup', function(e){
              Juno.file.monitor();
          });
        },
        save: function(file) {
            Juno.log('Saving file ('+file.doc+'):');
            Juno.log(Juno.get_document_contents(file.ref));
        },
        get_all: function() {
          Juno.log(Juno.files);
        },
        monitor: function() {
            $.each(Juno.files, function(k,v) {
                var old_buffer = v.buffer;
                var new_buffer = v.ref.getValue();
                var changes = old_buffer == new_buffer;
                if(!changes) {
                  Juno.log('updated buffer for: '+v.doc);
                  v.buffer = new_buffer;
                }
            });
        }
    },
    log: function(str) {
      if(console) {
        if(typeof(str) == 'string') {
          console.log('[JUNO]: '+str);
        } else {
          console.log('[JUNO]: Object:');
          console.log(str);
        }
      }
    },
    get_document_contents: function(ref) {
        return ref.getValue();
    },
    net: {
        api: {
          post_file_save: function() {
              // post to file saving api endpoint
          },
          post_file_save_working: function() {
              // post to working copy saving api endpoint
          }
        }
    }
};