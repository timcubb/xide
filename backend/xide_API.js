var fs = require("fs");
var restify = require('restify');

var THE_PATH = "/Users/cubbedgt/";
var THE_PORT = 8080;

var server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

/*

/get_dir - path
/get_file - path

/save_file - path, contents

/delete_file - path
/delete_dir - path

/rename_file - old_path, new_path
/rename_dir - old_path, new_path

/new_file - path
/new_dir - path


*/

server.post('/get_dir', function (req, res, next) {
	var directory_path = req.params.path;

	fs.readdir(THE_PATH+directory_path, function (err, data) {
		  if (err) throw err;
		  res.send(data);
		  return next();
	});
});

server.post('/get_file', function (req, res, next) {
	var directory_path = req.params.path;

	fs.readFile(THE_PATH+directory_path, function (err, data) {
		  if (err) throw err;
		  res.send(data);
		  return next();
	});
});

server.post('/save_file', function (req, res, next) {
	var filename = req.params.path;
	var contents = req.params.contents;

	fs.readdir(THE_PATH+filename, contents, function (err) {
		  if (err) throw err;
		  res.send(true);
		  return next();
	});
});


server.post('/delete_file', function (req, res, next) {
	var filename = req.params.path;

	fs.unlink(THE_PATH+filename, function (err) {
		  if (err) throw err;
		  res.send(true);
		  return next();
	});
});

server.post('/delete_dir', function (req, res, next) {
	var directory_path = req.params.path;

	fs.rmdir(THE_PATH+directory_path, function (err) {
		  if (err) throw err;
		  res.send(true);
		  return next();
	});
});


server.post('/delete_file', function (req, res, next) {
	var filename = req.params.path;

	fs.open(THE_PATH+filename, 'w', function (err) {
		  if (err) throw err;
		  res.send(true);
		  return next();
	});
});


server.post('/new_dir', function (req, res, next) {
	var directory_path = req.params.path;

	fs.mkdir(THE_PATH+directory_path, function (err) {
		  if (err) throw err;
		  res.send(true);
		  return next();
	});
});


server.post('/rename_file', function (req, res, next) {
	var old_path = req.params.old_path;
	var new_path = req.params.new_path;

	fs.unlink(THE_PATH+old_path, THE_PATH+new_path, function (err) {
		  if (err) throw err;
		  res.send(true);
		  return next();
	});
});


server.post('/rename_dir', function (req, res, next) {
	var old_path = req.params.old_path;
	var new_path = req.params.new_path;

	fs.rename(THE_PATH+old_path, THE_PATH+new_path, function (err) {
		  if (err) throw err;
		  res.send(true);
		  return next();
	});
});



server.listen(THE_PORT, function () {
  console.log('%s listening at %s', "xide", THE_PORT);
});


function is_directory(file) {
	fs.stat(file, function (err, stats) {
	  if (err) throw err;
	  return stats.isDirectory();
	});
}

function is_file(file) {
	fs.stat(file, function (err, stats) {
	  if (err) throw err;
	  return stats.isFile();
	});
}





/*

var Workspace = {};


Workspace.get_dir_contents = function() {
	fs.readdir(THE_PATH, function (err, data) {
	  if (err) throw err;
	  console.log(data);
	});
}

Workspace.get_file_contents = function(file) {
	fs.readFile(file, function (err, data) {
	  if (err) throw err;
	  return data;
	});
}

Workspace.write_file_contents = function(file, contents) {
	fs.writeFile(file, contents, function (err) {
	  if (err) throw err;
	  return true;
	});
}


Workspace.delete_file = function(file, contents) {
	fs.unlink(file, function (err) {
	  if (err) throw err;
	  return true;
	});
}


Workspace.delete_dir = function(file) {
	fs.rmdir(file, function (err) {
	  if (err) throw err;
	  return true;
	});
}

Workspace.new_file = function(file, contents) {
	fs.writeFile(file, contents, function (err) {
	  if (err) throw err;
	  return true;
	});
}


Workspace.new_dir = function(file) {
	fs.mkdir(file, function (err) {
	  if (err) throw err;
	  return true;
	});
}


Workspace.rename_file = function(oldf, newf) {
	fs.rename(oldf, newf, function (err) {
	  if (err) throw err;
	  return true;
	});
}


Workspace.rename_dir = function(oldd, newd) {
	fs.rename(oldd, newd, function (err) {
	  if (err) throw err;
	  return true;
	});
}


Workspace.is_directory = function(file) {
	fs.stat(file, function (err, stats) {
	  if (err) throw err;
	  return stats.isDirectory();
	});
}

Workspace.is_file = function(file) {
	fs.stat(file, function (err, stats) {
	  if (err) throw err;
	  return stats.isFile();
	});
}

*/